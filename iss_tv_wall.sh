#!/bin/bash

stream_buffer="queue2 use-buffering=true max-size-buffers=0 max-size-bytes=0 max-size-time=5000000000 use-tags-bitrate=true"

exec gst-launch-1.0 \
    glvideomixer name=m background=black \
        sink_0::xpos=0    sink_0::ypos=0   sink_0::width=1280 sink_0::height=720 sink_0::zorder=1 \
        sink_1::xpos=1200 sink_1::ypos=600 sink_1::width=720  sink_1::height=480 sink_1::zorder=0 \
        ! "video/x-raw(memory:GLMemory),width=1920,height=1080" ! glimagesink \
    filesrc location=<(streamlink https://www.ustream.tv/channel/iss-hdev-payload best -O) ! decodebin ! $stream_buffer ! glupload ! m.sink_0 \
    filesrc location=<(streamlink http://www.ustream.tv/channel/live-iss-stream best -O) ! decodebin ! $stream_buffer ! glupload ! m.sink_1

#exec gst-launch-1.0 \
#    compositor name=m background=black \
#        sink_0::xpos=0    sink_0::ypos=0   sink_0::width=1280 sink_0::height=720 sink_0::zorder=1 \
#        sink_1::xpos=1200 sink_1::ypos=600 sink_1::width=720  sink_1::height=480 sink_1::zorder=0 \
#        ! video/x-raw,width=1920,height=1080 ! autovideosink \
#    filesrc location=<(streamlink https://www.ustream.tv/channel/iss-hdev-payload best -O) ! decodebin ! $stream_buffer ! m.sink_0 \
#    filesrc location=<(streamlink http://www.ustream.tv/channel/live-iss-stream best -O) ! decodebin ! $stream_buffer ! m.sink_1
